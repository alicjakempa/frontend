#pragma once
#include<iostream>
#include"Zwierze.h"

using namespace std;

class Rasa : public Zwierze {

	string typ_rasy;
	
public:
	Rasa();
	Rasa(Zwierze, string);

	void setRasa(string);

	string getRasa();


};
