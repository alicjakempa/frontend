const list = document.querySelector('ul');
const form = document.querySelector('form');

const addRecipe = (recipe, id) => {
    let time = recipe.created_at.toDate();
    let html = `
    <li data-id="${id}">
        <div>${recipe.title}</div>
        <div>${time}</div>
        <button class="btn btn-danger btn-sm my-2">Delete</button>
    </li>
    `;
    list.innerHTML += html;
}
const deleteRecipe = (id) => {
    const recipes = document.querySelectorAll('li');
    recipes.forEach(recipe => {
        if(recipe.getAttribute('data-id') === id){
            recipe.remove();
        }
    })
};

//getting data from our database (collection)
db.collection('recipes').onSnapshot(snapshot => {
    snapshot.docChanges().forEach(change => {
        const doc = change.doc;
        if(change.type === 'added'){
            addRecipe(doc.data(), doc.id)
        } else if(change.type === 'removed'){
            deleteRecipe(doc.id);
        }
    })
});
//add documents/recipes
form.addEventListener('submit', e => {
    e.preventDefault();

    const now = new Date();
    const recipe = {
        //get a value created by user from input
        title: form.recipe.value,
        //using firestore library creating a timestamp object from new Date - now
        created_at: firebase.firestore.Timestamp.fromDate(now)
    };
    // adds a recipe to db - function add takes an OBJECT and is async function
    db.collection('recipes').add(recipe)
        .then(()=>{
            console.log('recipe added')
        })
        .catch(err => {
            console.log(err);
    });
})
//deleting data
list.addEventListener('click', e => {
    console.log(e);
    if(e.target.tagName === 'BUTTON'){
        const id = e.target.parentElement.getAttribute('data-id');
        db.collection('recipes').doc(id).delete()
            .then( () => {
             console.log('deleted');
        })
            .catch( err => console.log(err));
    }
})

