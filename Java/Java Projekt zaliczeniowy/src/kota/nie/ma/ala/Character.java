package kota.nie.ma.ala;

public abstract class Character extends Map{


    protected int XPosition;
    protected int YPosition;
    protected String CharacterName;

    public Character(int XPosition, int YPosition, String CharacterName) {
        this.XPosition = XPosition;
        this.YPosition = YPosition;
        this.CharacterName = CharacterName;
    }
    public Character () {}



    //////gettery////
    public int getXPosition() { return XPosition; }

    public int getYPosition() { return YPosition; }

    public void setXPosition(int XPosition) { this.XPosition = XPosition; }

    public void setYPosition(int YPosition) { this.YPosition = YPosition;}
}
