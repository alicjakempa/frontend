const addForm = document.querySelector('.add');
const list = document.querySelector('.todos');
const search = document.querySelector('.search input');

const template = todo => {

    //create element of html to adding to index.html
    const html = `
    <li class="list-group-item d-flex justify-content-between align-items-center">
        <span>${todo}</span>
        <i class="far fa-trash-alt delete"></i>
    </li>
    `;
    // place where to add new element
    list.innerHTML += html;
}

addForm.addEventListener('submit', e => {
    e.preventDefault();
    //take value from 'add' and trim it (there will be only string, no strange spaces)
    const todo = addForm.add.value.trim();

    if(todo.length){

        //call function
        template(todo);

        //reset 'input' to be empty after adding smth
        addForm.reset();
    }
    
});

//delete todos
list.addEventListener('click', e => {
    if(e.target.classList.contains('delete')){
        e.target.parentElement.remove();
    }
});
// function filters todos
// make an array from ul list, filter array and pick 'todos' NOT inluding 'term'
// give them a new class - which display none
const filterTodos = term => {

    Array.from(list.children)
    .filter( (todo) => !todo.textContent.toLowerCase().includes(term))
    .forEach( (todo) => todo.classList.add('filtered'));

    Array.from(list.children)
    .filter( (todo) => todo.textContent.toLowerCase().includes(term))
    .forEach( (todo) => todo.classList.remove('filtered'));
};

//search todos
search.addEventListener('keyup', () => {
    const term = search.value.trim().toLowerCase();
    filterTodos(term);
});