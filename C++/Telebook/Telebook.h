#pragma once
#include"Klasy_Telebook.h"


class Telebook {
	Osoba person;
	Data data_urodzenia;
	Praca job;
	Adres adress;


public:
	Telebook();
	Telebook(Osoba, Data);
	Telebook(Osoba, Data, Praca);
	Telebook(Osoba, Data, Praca, Adres);

	void setOsoba(Osoba);
	void setData_urodzenia(Data);
	void setJob(Praca);
	void setAdres(Adres);


	Osoba getOsoba();
	Data getData_urodzenia();
	Praca getJob();
	Adres getAdres();

};
