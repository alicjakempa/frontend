#pragma once

class Point {
protected:
	float x;
	float y;
public:
	Point();
	Point(float, float);
	Point(float);
	Point(float, float, int);

	void setX(float);
	void setY(float);

	float getX();
	float getY();

	void nr_cw();
	
	float distance(Point);

	void translate(float, float);

	/*
	
	*/
};