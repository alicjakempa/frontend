
<?php


include_once("connect.php");
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
	($_SESSION['zalogowany']))
	{

		$conn = polacz();

		
		if (isset($_POST['klient_id'])) {
			$klient_id = $_POST['klient_id'];
		}
		if (isset($_POST['imie'])) {
			$imie = $_POST['imie'];
		}
		if (isset($_POST['nazwisko'])) {
			$nazwisko = $_POST['nazwisko'];
		}
		if (isset($_POST['adres'])) {
			$adres = $_POST['adres'];
		}
		if (isset($_POST['phone_number'])) {
			$phone_number = $_POST['phone_number'];
		}
		if (isset($_POST['email'])) {
			$email = $_POST['email'];
		}
		$query = "INSERT INTO KLIENT(klient_id, imie, nazwisko) VALUES(:klient_id,:imie,:nazwisko)";

		$stm = oci_parse($conn,$query);
		
		oci_bind_by_name($stm, ':klient_id', $klient_id);
		oci_bind_by_name($stm, ':imie', $imie);
		oci_bind_by_name($stm, ':nazwisko', $nazwisko);
		
		if (oci_execute($stm))
		{	
			$query2 = "INSERT INTO KLIENT_ADRES(klient_id,adres) VALUES(:klient_id,:adres)";

			$stm2 = oci_parse($conn,$query2);
			oci_bind_by_name($stm2, ':klient_id', $klient_id);
			oci_bind_by_name($stm2, ':adres', $adres);

			if(oci_execute($stm2)){
				$query3 = "INSERT INTO KLIENT_KONTAKT(klient_id,phone_number, email) VALUES(:klient_id,:phone_number,:email)";
				$stm3 = oci_parse($conn,$query3);
				oci_bind_by_name($stm3, ':klient_id', $klient_id);
				oci_bind_by_name($stm3, ':phone_number', $phone_number);
				oci_bind_by_name($stm3, ':email', $email);

				if(oci_execute($stm3)){
					header("Location: show.php");
					//close ...
					die();
				}

			}
					
		}

		oci_free_statement($stm);
		oci_commit($conn);
		oci_close($conn);
	}
	else {

		header("Location: loginf.php");
	}

?>

