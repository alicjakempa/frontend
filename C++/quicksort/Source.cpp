#include <iostream>

using namespace std;

void quicksort(int *T, int d, int g);

int main() {
	cout << "Sortowanie szybkie" << endl;
	cout << "Podaj wielkość tablicy do posortowania: ";
	int n;
	cin >> n;

	//dynamiczna deklaracja tablicy T o wielkosci n
	int *T = new int[n];

	// wypełnienie tablicy  wartosciami
	for (int i = 0; i < n; i++) {
		T[i] = rand(); // wartosci losowe
		//T[i] = i; // wartosci rosnace
		//T[i] = 1; // wartosci stale
	}
	cout << "Tablica przed sortowaniem ";
	for (int i = 0; i < n; i++) {
		cout << T[i] << " ";
	
	}

	//wywołanie funkcji sortujacej dla calej tablicy
	quicksort(T, 0, n - 1);
	
	// wyswietlenie wynikow, o ile n <= 1000
	if (n <= 1000) {
		cout << "\nTablica po posortowaniu: ";
		for (int i = 0; i < n; i++) {
			cout << T[i] << " ";
		}
		cout << endl;
	}

	system("PAUSE");
	return 0;
}



int podzial(int *T, int d, int g)
{
	int element_graniczny = T[d]; 
	int i = d;
	int j = g;
	int tmp;
	while (true)
	{
		while (T[j] > element_graniczny)
			j--;

		while (T[i] < element_graniczny)
			i++;

		if (i < j) {
			tmp = T[i];
			T[i] = T[j];
			T[j] = tmp;

			i++;
			j--;
		}
		else return j;
	}
}
void quicksort(int *T, int d, int g) {
	int p;
	if (d < g) {
		p = podzial(T, d, g);
		quicksort(T, d, p);
		quicksort(T, p + 1, g);
	}
}


/*
	
int podzial(int *T, int d, int g) {

	int element_graniczny = T[d];		//miejsce elementu granicznego - tutaj jako pierwszy element tablicy - wartosc !
	int srodek = d;						// numer elementu, ktory jest srodkiem - tutaj nr d czyli 0 element w tablicy
	int j = g;
	int tmp;


	for (int i = d+1; i <= g; i++ ){
		
		if (T[i] < element_graniczny) {

			T[tmp] == T[srodek];
			T[srodek] == T[i];
			T[d] == T[tmp];
			srodek = srodek + 1;
		}
		else return d;
	}
}

void quicksort(int *T, int d, int g) {
	int p;
	if (d < g) {
		p = podzial(T, d, g);
		quicksort(T, d, p);
		quicksort(T, p + 1, g);
	}
}
*/
	
		
	
//wyklad 2 str 8