<?php


include_once("connect.php");
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
	($_SESSION['zalogowany']))
	{

		$conn = polacz();

		
		if (isset($_POST['jacht_id'])) {
			$jacht_id = $_POST['jacht_id'];
		}
		
		if (isset($_POST['nazwa'])) {
			$nazwa = $_POST['nazwa'];
		}
		if (isset($_POST['jacht_szczegoly_id'])) {
			$jacht_szczegoly_id = $_POST['jacht_szczegoly_id'];
        }
        
		$query = "INSERT INTO JACHT(jacht_id, nazwa, jacht_szczegoly_id) VALUES(:jacht_id, :nazwa, :jacht_szczegoly_id)";
		$stm = oci_parse($conn,$query);
        
        oci_bind_by_name($stm, ':jacht_id', $jacht_id);
		oci_bind_by_name($stm, ':nazwa', $nazwa);
		oci_bind_by_name($stm, ':jacht_szczegoly_id', $jacht_szczegoly_id);
		
		if (oci_execute($stm))
		{		
			header("Location: lista.php");
			die();		
		}

		oci_free_statement($stm);
		oci_commit($conn);
		oci_close($conn);
	}
	else {

		header("Location: loginf.php");
	}

?>