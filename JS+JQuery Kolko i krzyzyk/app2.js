const PLAYER1 = 'X'
const PLAYER2 = 'O'

$(document).ready(function(){
    const grid = [
        [' ', ' ', ' '],
        [' ', ' ', ' '],
        [' ', ' ', ' ']
    ];
    
    function gameOver(){
        for (var i=0; i<3; i++){
            if(grid[i][0] !==' ' &&
            grid[i][0] === grid[i][1] &&
            grid[i][0] === grid[i][2] ){
                return grid[i][0];
            }
        }
        for (var j=0; j<3; j++){
            if(grid[0][j] !==' ' &&
            grid[0][j] === grid[1][j] &&
            grid[0][j] === grid[2][j] ){
                return grid[0][j];
            }
        }
        if(grid[0][0] !== ' ' &&
            grid[0][0] === grid[1][1] &&
            grid[0][0] === grid[2][2] ){
            return grid[0][0];
            }
        if(grid[0][2] !== ' ' &&
            grid[0][2] === grid[1][1] &&
            grid[0][2] === grid[2][0] ){
            return grid[0][2];
            }
        
        return null;   
    }

    movePlayer1();
    
    function movePlayer1(){
        $('.box').click(function(){
            $(this).html(PLAYER1);
            let row = $(this).data('row');
            let column = $(this).data('column');
            
                grid[row][column] = PLAYER1;
                console.log(grid);
                  
            let gameStatus = gameOver();
            if (gameStatus != null){
                alert('game over: ' + gameStatus);
                close.movePlayer();
                
            } else {
                movePlayer2();
            }
        });   
    }
    function movePlayer2(){
        $('.box').click(function(){
            $(this).html(PLAYER2);
            let row2 = $(this).data('row');
            let column2 = $(this).data('column');

            grid[row2][column2] = PLAYER2;
            console.log(grid);
                
            let gameStatus = gameOver();
            if (gameStatus != null){
                alert('game over: ' + gameStatus);  
                close.movePlayer2();
                
            } else {
                movePlayer1();
            }
        });
    }
    
    $('#restart').click(function(){
        for(var i=0; i<3; i++){
            for(var j=0; j<3; j++){
                grid[i][j] = ' ';
                $('.box[data-row=' + i + '][data-column=' + j + ']').html(' ');
                movePlayer1();
            }            
        }  
    });  
});
