

<?php

include_once("connect.php");
session_start();
if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany'])){

	$conn = polacz();
	
	$klient_id = $_GET['klient_id'];
	$imie = $_POST['imie'];
	$nazwisko = $_POST['nazwisko'];
	$adres = $_POST['adres'];
	$phone_number = $_POST['phone_number'];
	$email = $_POST['email'];

	$query = "UPDATE klient SET IMIE=:imie, NAZWISKO=:nazwisko WHERE KLIENT_ID=:klient_id";
	$stm = oci_parse($conn, $query);
	
	oci_bind_by_name($stm, ':klient_id', $klient_id);
	oci_bind_by_name($stm, ':imie', $imie);
	oci_bind_by_name($stm, ':nazwisko', $nazwisko);
	
		if (oci_execute($stm, OCI_NO_AUTO_COMMIT))
		{	
			$query2 = "UPDATE klient_adres SET ADRES=:adres WHERE KLIENT_ID=:klient_id";
			$stm2 = oci_parse($conn, $query2);
	
			oci_bind_by_name($stm2, ':klient_id', $klient_id);
			oci_bind_by_name($stm2, ':adres', $adres);

			if(oci_execute($stm2, OCI_NO_AUTO_COMMIT)){

				$query3 = "UPDATE klient_kontakt SET PHONE_NUMBER=:phone_number, EMAIL=:email WHERE KLIENT_ID=:klient_id";
				$stm3 = oci_parse($conn, $query3);
	
				oci_bind_by_name($stm3, ':klient_id', $klient_id);
				oci_bind_by_name($stm3, ':phone_number', $phone_number);
				oci_bind_by_name($stm3, ':email', $email);

				if(oci_execute($stm3, OCI_NO_AUTO_COMMIT)){

					header("Location: show.php");
				}
			}		
		}
		else {
		echo "błąd krytyczny";
		}
	oci_free_statement($stm);
	oci_commit($conn);
	oci_close($conn);	
}
?>