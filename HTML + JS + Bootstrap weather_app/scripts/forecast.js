class Forecast{
    constructor(){
        this.key = 'c3Rag7M8htDQG14lA6od2xzKzlTSSVuf';
        this.weatherURI = 'http://dataservice.accuweather.com/currentconditions/v1/';
        this.cityURI = 'http://dataservice.accuweather.com/locations/v1/cities/search';
    }
    async updateCity(city){
        const cityDets = await this.getCity(city);
        const weather = await this.getWeather(cityDets.Key);

        return {
            // cityDets: cityDets,
            // weather: weather
            //in short notation, because names are the same we can do this like that:
            cityDets,
            weather
        };
    }
    async getCity(city){
        //data from accuweather
        const query = `?apikey=${this.key}&q=${city}`;
        //works exacly like in a browser
        const response = await fetch(this.cityURI + query);
        //it will be: http://dataservice.accuweather.com/locations/v1/cities/search?apikey=	c3Rag7M8htDQG14lA6od2xzKzlTSSVuf&q=menchester
        //and then saving as a json
        const data = await response.json();
        //returning first position
        return data[0];
    }
    //get weather information
    async getWeather(id){
        const query = `${id}?apikey=${this.key}`;
        const response = await fetch(this.weatherURI + query);
        const data = await response.json();
        return data[0];
    }
}


