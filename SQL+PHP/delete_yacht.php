<?php
include_once ('connect.php');
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany']))
    {   
   
	$conn = polacz();
	
	$id = $_GET['jacht_id'];
	
	$query  = "SELECT * FROM REZERWACJE WHERE JACHT_ID=:id";
	$stm = oci_parse($conn, $query);
	oci_bind_by_name($stm, ':id', $id);
	
	oci_execute($stm);
		
		if($row = oci_fetch_array($stm)){
		echo "Na podany jacht zrobione są rezerwacje. Zmodyfikuj je przed usunięciem. <br><br>Lista rezerwacji:<br><br>";
		echo "ID REZERWACJI: <b>".$row[0]."</b>    DATA WYNAJMU: ".$row[3]." ".$row[4]."<br>";
		echo "<a href='rezerwacje.php'>Zarządzanie rezerwacjami</a>";
		}
		else{

			$query2 = "DELETE JACHT WHERE JACHT_ID=:id";
			
			$stm2 = oci_parse($conn,$query2);
			
			oci_bind_by_name($stm2, ':id', $id);
		
			if (oci_execute($stm2))
			{	
				header("Location: lista.php");
				die();			
			}
		}
}

else{
	echo "Błąd krytyczny";
}



?>

