
<?php

include_once("connect.php");
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
	($_SESSION['zalogowany']))
	{
        
		$conn = polacz();

		
		if (isset($_POST['rezerwacje_id'])) {
		$rezerwacje_id = $_POST['rezerwacje_id'];
		}
		if (isset($_POST['klient_id'])) {
		$klient_id = $_POST['klient_id'];
		}
		if (isset($_POST['jacht_id'])) {
		$j_id = $_POST['jacht_id'];
		}
		if (isset($_POST['start_rezerwacji'])) {
		$start = $_POST['start_rezerwacji'];
		}
		if (isset($_POST['koniec_rezerwacji'])) {
		$koniec = $_POST['koniec_rezerwacji'];
		}

		
	

	//---- TU POWINNO BYĆ SPRAWDZENIE ZAPOBIEGAJĄCE DODANIA KOLIDUJĄCEJ REZERWACJI---//
	//NIESTETY NIE DZIAŁA. PO KILKU DNIACH PRÓB ZNALEZIENIA BŁĘDU/ROZWIĄZANIA SIĘ PODDALIŚMY//
		
		
		$quer="SELECT REZERWACJE_ID, KLIENT_ID, JACHT_ID, START_REZERWACJI, KONIEC_REZERWACJI FROM REZERWACJE
		WHERE JACHT_ID=:j_id AND 
		('TO_DATE(''$start'', ''YYYY-MM-DD'')' BETWEEN START_REZERWACJI AND KONIEC_REZERWACJI) OR
		('TO_DATE(''$koniec'', ''YYYY-MM-DD'')' BETWEEN START_REZERWACJI AND KONIEC_REZERWACJI)";

		$stmt = oci_parse($conn, $quer);

		oci_bind_by_name($stmt, ':j_id', $j_id);
		oci_bind_by_name($stmt, ':start', $start);
		oci_bind_by_name($stmt, ':koniec', $koniec);
        
		if(oci_execute($stmt)){
			
			$row = oci_fetch_array($stmt);
			echo "Istnieje już rezerwacja na ten jacht w nachodzących na siebie terminach. Sprawdź rezerwacje. <br><br>";
			echo "ID rezerwacji:<b> ".$row[0]."</b> Początek rezerwacji:<b> ".$row[3]."</b> Koniec rezerwacji: <b>".$row[4]."</b><br><br>";

			echo "<a href='rezerwacje.php'>ZARZĄDZANIE REZERWACJAMI </a>";	
		}
	//-------------------------------------------------------------------------------------------------//
        else {

		$query = "INSERT INTO REZERWACJE(REZERWACJE_ID, KLIENT_ID, JACHT_ID, START_REZERWACJI, KONIEC_REZERWACJI) 
        VALUES(:rezerwacje_id, :klient_id, :j_id, TO_DATE('$start','YYYY-MM-DD'), 
		TO_DATE('$koniec','YYYY-MM-DD'))";
		
		$stm = oci_parse($conn,$query);
        
        oci_bind_by_name($stm, ':rezerwacje_id', $rezerwacje_id);
		oci_bind_by_name($stm, ':klient_id', $klient_id);
		oci_bind_by_name($stm, ':j_id', $j_id);
        oci_bind_by_name($stm, ':start', $start);
        oci_bind_by_name($stm, ':koniec', $koniec);
		
		if (oci_execute($stm))
		{		
			header("Location: rezerwacje.php");
			die();		
		}

		oci_free_statement($stm);
		oci_commit($conn);
		oci_close($conn);
	}
}
	
else {
    header("Location: loginf.php");
}

?>

