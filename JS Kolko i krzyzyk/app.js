const PLAYER1 = 'fa-circle'; // gracz nieparzystych rund, klasa kolko
const PLAYER2 = 'fa-times'; // gracz parzystych rund, klasa krzyzyk
let round = 1;

// Tablica zapisująca wyniki ruchu
const board = [
    ['', '', ''],
    ['', '', ''],
    ['', '', ''],
];
// Wygrywające kombinacje:
const combinations = [
    [0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]
];

/* Zamiana w tablicę umożliwiającą używanie forEach */ 
const boxes = [...document.querySelectorAll('.box')];

/* Nastawienie nasłuchu na kliknięcie w pole - box  */ 
boxes.forEach(box => box.addEventListener('click', pick));

/* Fukcja pick wykonywana po kliknięciu. 
Sprawdza czy runda jest parzysta i przypisuje do niej klasę danego gracza.
Zapisuje ruch to tablicy board.*/ 
function pick(event){

    const {row, column} = event.target.dataset;
    const turn = round % 2 === 0 ? PLAYER2 : PLAYER1;

    if(board[row][column] !== '') return;

    event.target.classList.add(turn);
    board[row][column] = turn;

    round++;

    console.log(check()); // funkcja sprawdzi czy juz nie wygralismy
}
function check(){

    const result = board.reduce((total,row) => total.concat(row));
    let winner = null;
    let moves = {
        'fa-times': [],
        'fa-circle': []
    };
    result.forEach((field, index) => moves[field] ? moves[field].push(index): null);
    combinations.forEach(combination => {
        if(combination.every(index => moves[PLAYER1].indexOf(index) > -1)){
            winner = 'Winner: Player 1';
        }
        if(combination.every(index => moves[PLAYER2].indexOf(index) > -1)){
            winner = 'Winner: Player 2';
        }
    });
    return winner;
}