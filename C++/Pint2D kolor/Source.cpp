#include<iostream>
#include<string>
#include"Point.h"
#include"ColourPoint.h"

using namespace std;


int main() {

	Point my_point(3.3, 4.4);

	cout << "wsp. x obiektu my_point " << my_point.getX() << endl;
	cout << "wsp. y obiektu my_point " << my_point.getY() << endl;

	cout << "-----------------------\n";

	ColourPoint my_colour_point;

	cout << "wsp. x obiektu my_colour_point " << my_colour_point.getX() << endl;
	cout << "wsp. y obiektu my_colour_point " << my_colour_point.getY() << endl;
	cout << "Kolor obiektu my_colour_point " << my_colour_point.getColour() << endl;

	cout << "-----------------------\n";

	ColourPoint new_colour_point("green");

	cout << "wsp. x obiektu new_colour_point " << new_colour_point.getX() << endl;
	cout << "wsp. y obiektu new_colour_point " << new_colour_point.getY() << endl;
	cout << "Kolor obiektu new_colour_point " << new_colour_point.getColour() << endl;

	cout << "-----------------------\n";

	ColourPoint very_new_colour_point(1.111, 2.222, "yellow");

	cout << "wsp. x obiektu very_new_colour_point " << very_new_colour_point.getX() << endl;
	cout << "wsp. y obiektu very_new_colour_point " << very_new_colour_point.getY() << endl;
	cout << "Kolor obiektu very_new_colour_point " << very_new_colour_point.getColour() << endl;

	cout << "-----------------------\n";

	ColourPoint colour_point(my_point, "blue");

	cout << "wsp. x obiektu colour_point " << colour_point.getX() << endl;
	cout << "wsp. y obiektu colour_point " << colour_point.getY() << endl;
	cout << "Kolor obiektu colour_point " << colour_point.getColour() << endl;

	cout << "-----------------------\n";
	
	my_point.translate(2, 3);
	colour_point.translate(2, 3);
	
	cout << "wsp. x obiektu my_point " << my_point.getX() << endl;
	cout << "wsp. y obiektu my_point " << my_point.getY() << endl;

	cout << "wsp. x obiektu colour_point " << colour_point.getX() << endl;
	cout << "wsp. y obiektu colour_point " << colour_point.getY() << endl;
	cout << "Kolor obiektu colour_point " << colour_point.getColour() << endl;

	cout << "TUTAAAAJ-----------------------\n";

	colour_point.Point::translate(2, 3);

	cout << "wsp. x obiektu colour_point " << colour_point.getX() << endl;
	cout << "wsp. y obiektu colour_point " << colour_point.getY() << endl;
	cout << "Kolor obiektu colour_point " << colour_point.getColour() << endl;

	my_point = colour_point;

	cout << "wsp. x obiektu my_point " << my_point.getX() << endl;
	cout << "wsp. y obiektu my_point " << my_point.getY() << endl;

	//colour_point = my_point;

	system("pause");
	return 0;
}