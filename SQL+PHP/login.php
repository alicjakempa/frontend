
<?php

/* including data connection*/ 
include_once("connect.php");

/*starts a new session*/ 
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany'])
    ){
        header("Location: index.html");
    }
    else {
    
    $_SESSION['zalogowany'] = false;
    $c = polacz();

    /* Catching login and password from html form*/ 

    if ( (isset($_POST['login'])) && (isset($_POST['pass'])))
    {
        $login = $_POST['login'];
        $pass = $_POST['pass'];
        $qu="SELECT ID FROM admin WHERE pass=DBMS_CRYPTO.hash(UTL_RAW.CAST_TO_RAW(:pass),3)
        AND login=:login";
        $stmt = oci_parse($c, $qu);
        oci_bind_by_name($stmt,':pass',$pass);
        oci_bind_by_name($stmt,':login',$login);
        if(!oci_execute($stmt))
        {
            $e = oci_error();
            echo $e['message'];
        }
        $r = oci_fetch_array($stmt);  
        print_r($r);
        if ($r)
        {
            $_SESSION['zalogowany'] = true;
            $_SESSION['login'] = $login;
            $_SESSION['id'] = $r['ID'];
            header("Location: index.html");
            die();
        }
        header("Location: loginf.php");   
    }
}

?>