#pragma once

class Data {

	int rok;
	int miesiac;
	int dzien;

public:
	Data();
	Data(int _dzien, int _miesiac, int _rok);

	void setRok(int);
	void setMiesiac(int);
	void setDzien(int);

	int getRok();
	int getMiesiac();
	int getDzien();

};