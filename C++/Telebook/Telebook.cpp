#include "Telebook.h"

Telebook::Telebook()
{
	cout << "Dziala konstruktor klasy Telebook bez arg.\n";
}

Telebook::Telebook(Osoba _person, Data _data_urodzenia):person(_person), data_urodzenia(_data_urodzenia)
{
	cout << "Dziala konstruktor klasy Telebook z argumentami\n";
}

Telebook::Telebook(Osoba _person, Data _data_urodzenia, Praca _job):person(_person), data_urodzenia(_data_urodzenia), job(_job)
{
	cout << "Dzia�a konstruktor klasy Telebook z 3 argumentami\n";
}

Telebook::Telebook(Osoba _person, Data _data_urodzenia, Praca _job, Adres _adress):person(_person), data_urodzenia(_data_urodzenia), job(_job), adress(_adress)
{
}


void Telebook::setOsoba(Osoba _person) {person = _person;}

void Telebook::setData_urodzenia(Data _data_urodzenia) { data_urodzenia = _data_urodzenia; }

void Telebook::setJob(Praca _job) { job = _job; }

void Telebook::setAdres(Adres _adress) { adress = _adress; }


Osoba Telebook::getOsoba() {return person;}

Data Telebook::getData_urodzenia() {return data_urodzenia;}

Praca Telebook::getJob() {return job;}

Adres Telebook::getAdres() {return adress;}
