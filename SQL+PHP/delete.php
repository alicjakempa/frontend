<?php
include_once ('connect.php');
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany']))
    {   
   
	$conn = polacz();
	
	$id = $_GET['klient_id'];

	$q = "SELECT * FROM REZERWACJE WHERE KLIENT_ID=:klient_id";
	$s = oci_parse($conn,$q);
	oci_bind_by_name($s, ':id', $id);

	if(oci_execute($s)){
		echo "Nie można usunąć użytkownika.<br>
		Na danego użytkownika zrobiona jest rezerwacja.<br>
		W celu usunięcia użytkownika należy zmienić lub usunąć rezerwacje,
		a następnie wrócić do panelu zarządzania klientem.<br>
          <br><br>Lista rezerwacji przypisanych do usuwanego użytkownika:<br><br>";
		$r = oci_fetch_array($s);
		echo "ID REZERWACJI: <b>".$r[0]."</b>    JACHT: ".$r[2]." TERMIN REZERWACJI: ".$r[3]." - ".$r[4]."<br><br>";
		echo "<a href='rezerwacje.php'>Zarządzanie rezerwacjami</a>";
	}
	else{


		$query = "DELETE KLIENT_ADRES WHERE klient_id=:id";
		
		$stm = oci_parse($conn,$query);
		
		oci_bind_by_name($stm, ':id', $id);
		
		
		if (oci_execute($stm))
		{	
			$query2 = "DELETE KLIENT_KONTAKT WHERE klient_id=:id";
			$stm2 = oci_parse($conn,$query2);
			oci_bind_by_name($stm2, ':id', $id);

			if (oci_execute($stm2)){
				$query3 = "DELETE KLIENT WHERE klient_id=:id";
				$stm3 = oci_parse($conn,$query3);
				oci_bind_by_name($stm3, ':id', $id);

				if (oci_execute($stm3)){
					header("Location: show.php");
				
				die();
				}
			}			
		}
	}
}
else
{
	echo "Błąd krytyczny";
}


?>