#include "Rasa.h"

Rasa::Rasa()
{
	typ_rasy = "brak";
	cout << "Dziala konstruktor klasy Rasa bez arg. " << endl;
}

Rasa::Rasa(Zwierze zwierze, string _typ_rasy)
{
	imie = zwierze.getImie();
	nr_chip = zwierze.getNr_chip();
	typ_rasy = _typ_rasy;
}


void Rasa::setRasa(string _rasa)
{
	typ_rasy = _rasa;
}

string Rasa::getRasa()
{
	return typ_rasy;
}
