#pragma once
#include<iostream>

using namespace std;

class Zwierze {
protected:
	string imie;
	int nr_chip;

public:
	Zwierze();
	Zwierze(string _imie, int _nr_chip);

	void setImie(string);
	void setNr_chip(int);

	string getImie();
	int getNr_chip();

};
