#pragma once
#include "Osoba.h"


Osoba::Osoba() : imie("Jan"), nazwisko("Kowalski")
{
	cout << "Dziala konstruktor klasy Osoba bez arg.\n";	
}

Osoba::Osoba(string _imie, string _nazwisko)
{
	cout << "Dziala konstruktor klasy Osoba z argumentami\n";
	imie = _imie;
	nazwisko = _nazwisko;

}

void Osoba::setImie(string _imie) { imie = _imie; }

void Osoba::setNazwisko(string _nazwisko) { nazwisko = _nazwisko; }

string Osoba::getImie() {return imie;}

string Osoba::getNazwisko(){return nazwisko;}
