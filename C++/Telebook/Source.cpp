#include<iostream>
#include<string>
#include"Klasy_Telebook.h"
#include"Telebook.h"


using namespace std;

int main() {

	/*Data moja_data;

	cout << "Domyslny rok urodzenia z klasy Data \n" << moja_data.getRok() << endl;

	Data nowa_data(1, 1, 2000);
	cout << "Rok urodzenia z obiektu nowa_data \n" << nowa_data.getRok() << endl;

	Osoba moja_osoba;

	cout << "Imie z obiektu moja_osoba \n" << moja_osoba.getImie() << endl;

	Osoba nowa_osoba("Alicja", "Kempa");

	cout << "Imie z obiektu nowa_osoba \n" << nowa_osoba.getImie() << endl;

	*/cout << "------------------------\n";

	Telebook moja_ksiazka;

	Telebook moja_ksiazka2(Osoba ("Mariusz", "Zyg"), Data(1,12,2000) );

	cout << moja_ksiazka2.getOsoba().getImie() << endl;
	cout << moja_ksiazka2.getOsoba().getNazwisko() << endl;
	cout << moja_ksiazka2.getData_urodzenia().getRok() << endl;

	moja_ksiazka2.setData_urodzenia(Data(3, 03, 2005));
	cout << moja_ksiazka2.getData_urodzenia().getRok() << endl;

	cout << "------------------------\n";

	Telebook moja_ksiazka3(Osoba("Krzysztof ", "Basista "), Data(6, 8, 1992), Praca("Nava ", "The Best Digitalization Manager "));
	cout << moja_ksiazka3.getOsoba().getImie() << " ";
	cout << moja_ksiazka3.getOsoba().getNazwisko() << endl;
	cout << moja_ksiazka3.getJob().getNazwa_zakladu() << " ";
	cout << moja_ksiazka3.getJob().getZawod() << endl;

	cout << "------------------------\n";

	Telebook nowy_obiekt(Osoba("Alicja", "Kempa"), Data(1, 1, 1992), Praca("szkjdlf", "zshdbjvk"), Adres("Gdansk", "Powstancow", 4, 13));
	cout << nowy_obiekt.getAdres().getMiasto() << " ";
	cout << nowy_obiekt.getAdres().getUlica() << " ";
	cout << nowy_obiekt.getAdres().getNr_budynku() << " ";
	cout << nowy_obiekt.getAdres().getNr_mieszkania() << endl;
	nowy_obiekt.setAdres(Adres("Malbork", "Dybowskiego", 20, 0));
	cout << nowy_obiekt.getAdres().getMiasto() << " ";
	cout << nowy_obiekt.getAdres().getUlica() << " ";
	cout << nowy_obiekt.getAdres().getNr_budynku() << " ";
	cout << nowy_obiekt.getAdres().getNr_mieszkania() << endl;

	
	/*
	cout << "Imie osoby z obiektu moja_ksiazka: " << \
		moja_ksiazka.getOsoba().getImie() << endl;

	cout << "Rok urodzenia osoby z obiektu moja_ksiazka: " << \
		moja_ksiazka.getData_urodzenia().getRok() << endl;

	cout << "------------------------\n";

	
	//wprowadzam dane

	cout << "Podaj imie \n";
	string tmp_imie;
	cin >> tmp_imie;

	Telebook inna_ksiazka(Osoba(tmp_imie, "Kowalski"), Data(6,8,1992));

	cout << "Imie osoby z obiektu inna_ksiazka: " << \
	inna_ksiazka.getOsoba().getImie() << endl;

	cout << "Rok urodzenia osoby z obiektu inna_ksiazka: " << \
	inna_ksiazka.getData_urodzenia().getRok() << endl;
	
	Telebook nowa_ksiazka(nowa_osoba, nowa_data);

	cout << "Imie osoby z obiektu nowa_ksiazka: " << \
		nowa_ksiazka.getOsoba().getImie() << endl;

	cout << "Rok urodzenia osoby z obiektu nowa_ksiazka: " << \
		nowa_ksiazka.getData_urodzenia().getRok() << endl;

	Telebook tablica[10];
	tablica[3].getData_urodzenia().getRok();
	
	*/

	system("pause");
	return 0;
}