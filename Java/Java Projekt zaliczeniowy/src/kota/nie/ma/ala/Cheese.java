package kota.nie.ma.ala;

public class Cheese extends Character {

   private String Charactername = "G "; // czyli gouda

   private int XPosition = getMap_center();
   private int YPosition = getMap_center();


   public Cheese(int XPosition, int YPosition, String Charactername) {
      super(XPosition, YPosition, "G ");
   }

   public Cheese(){};

   ////////////////gettery i settery//////////////////


   public String getCharactername() { return Charactername; }

   public int getXPosition() { return XPosition; }

   public int getYPosition() { return YPosition; }

}