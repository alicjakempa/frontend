<?php

include_once("connect.php");
session_start();
if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany'])){

	$conn = polacz();
	
	
    $jacht_szczegoly_id = $_GET['jacht_szczegoly_id'];
    
	if (isset($_POST['rodzaj'])) {
		$rodzaj = $_POST['rodzaj'];}
	if (isset($_POST['dlugosc'])) {
        $dlugosc = $_POST['dlugosc'];}
    if (isset($_POST['ilosc_lozek'])) {
        $ilosc_lozek = $_POST['ilosc_lozek'];}
    if (isset($_POST['opis'])) {
        $opis = $_POST['opis'];}

	$query = "UPDATE JACHT_SZCZEGOLY SET RODZAJ=:rodzaj, DLUGOSC=:dlugosc, ILOSC_LOZEK=:ilosc_lozek, OPIS=:opis 
    WHERE JACHT_SZCZEGOLY_ID=:jacht_szczegoly_id";

	$stm = oci_parse($conn, $query);
	
    oci_bind_by_name($stm, ':jacht_szczegoly_id', $jacht_szczegoly_id);
    oci_bind_by_name($stm, ':rodzaj', $rodzaj);
    oci_bind_by_name($stm, ':dlugosc', $dlugosc);
    oci_bind_by_name($stm, ':ilosc_lozek', $ilosc_lozek);
    oci_bind_by_name($stm, ':opis', $opis);

		if (oci_execute($stm,OCI_NO_AUTO_COMMIT))
		{	
			header("Location: lista.php");
							
		}
		else {
		echo "błąd krytyczny";
		}
	oci_free_statement($stm);
	oci_commit($conn);
	oci_close($conn);	
}
?>