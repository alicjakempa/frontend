#pragma once
#include "Data.h"
#include<iostream>

using namespace std;

Data::Data()
{
	cout << "Dziala konstruktor klasy Data bez arg.\n";
	dzien = 1;
	miesiac = 1;
	rok = 1900;
}

Data::Data(int _dzien, int _miesiac, int _rok) : dzien(_dzien), miesiac(_miesiac), rok(_rok)
{
	cout << "Dziala konstruktor klasy Data z argumentami\n";
	//dzien = _dzien; - nazwy sie nie gryza
}

void Data::setRok(int r) {rok = r;}

void Data::setMiesiac(int m) {miesiac = m;}
 
void Data::setDzien(int dz) {dzien = dz;}

int Data::getRok() {return rok;}

int Data::getMiesiac() {return miesiac;}

int Data::getDzien() {return dzien;}

