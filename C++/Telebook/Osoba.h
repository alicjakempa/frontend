#pragma once
#include<iostream>

using namespace std;

class Osoba {

	string imie;
	string nazwisko;

public:
	Osoba();
	Osoba(string _imie, string _nazwisko);

	void setImie(string);
	void setNazwisko(string);

	string getImie();
	string getNazwisko();


};