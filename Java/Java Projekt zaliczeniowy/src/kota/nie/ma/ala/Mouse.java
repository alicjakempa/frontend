package kota.nie.ma.ala;

import javax.naming.Name;

public class Mouse extends Character implements Moveable {

    private String CharacterName = "M";
    private int XPosition = randomPosition();
    private int YPosition = randomPosition2();

    public Mouse() {
    }

    public Mouse(int XPosition, int YPosition, String CharacterName) {
        super(XPosition, YPosition, CharacterName);
    }

    int randomPosition() {
        int newPosition = ((int) (Math.random() * 1000)) % getMap_size();
        return newPosition;
    }

    int randomPosition2() {
        if (XPosition == 0 | XPosition == getMap_size() - 1) {
            int newPosition2 = ((int) (Math.random() * 1000)) % getMap_size();
            return newPosition2;
        } else {
            int newPosition2 = ((int) (Math.random() * 1000)) % 2;
            if (newPosition2 == 1) {
                newPosition2 = 8;
            }
            return newPosition2;
        }
    }

    public void move() {
        int vector = ((int) (Math.random() * 1000)) % 9;

        int tmpXPosition = XPosition;
        int tmpYPosition = YPosition;


        if (vector == 0) {YPosition = YPosition + 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }} // w gore

        if (vector == 1) {XPosition = XPosition + 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }} // w prawo

        if (vector == 2) {YPosition = YPosition - 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }} // w dol

        if (vector == 3) {XPosition = XPosition - 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }} // w lewo

        if (vector == 4) {XPosition = XPosition + 1; YPosition = YPosition + 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }} // gora-prawo

        if (vector == 5) {XPosition = XPosition + 1; YPosition = YPosition - 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }} // dol prawo

        if (vector == 6) {XPosition = XPosition - 1; YPosition = YPosition - 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }}// dol lewo

        if (vector == 7) {XPosition = XPosition - 1; YPosition = YPosition + 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }}// gora lewo

        if (vector == 8) {XPosition = XPosition + 0; YPosition = YPosition + 0; }// brak ruchu

        setXPosition(XPosition);
        setYPosition(YPosition);
}

    //////gettery//////

    public void setCharacterName(String characterName) { CharacterName = characterName; }

    public String getCharacterName() { return CharacterName; }

    public int getXPosition() { return XPosition; }

    public int getYPosition() { return YPosition; }
}






