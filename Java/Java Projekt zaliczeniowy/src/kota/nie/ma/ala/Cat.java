package kota.nie.ma.ala;

public class Cat extends Character implements Moveable{


    private String Charactername = "C"; // czyli CAT
    private int XPosition = randomPosition();
    private int YPosition = randomPosition();


    public Cat(){}

    public Cat(int XPosition, int YPosition, String CharacterName) {
        super(XPosition, YPosition, CharacterName);
    }

    int randomPosition() {
        int newPosition = ((int)(Math.random()*1000))% getMap_size();
        return newPosition;
    }


    public void move() {
        int vector = ((int) (Math.random() * 1000)) % 5;

        int tmpXPosition = XPosition;
        int tmpYPosition = YPosition;


        if (vector == 0) {YPosition = YPosition + 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1) {
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }} // w gore

        if (vector == 1) {XPosition = XPosition + 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }} // w prawo

        if (vector == 2) {YPosition = YPosition - 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }} // w dol

        if (vector == 3) {XPosition = XPosition - 1;
            if (XPosition < 0 | XPosition > getMap_size()-1 | YPosition < 0 | YPosition > getMap_size()-1){
                XPosition = tmpXPosition; YPosition = tmpYPosition;
                move(); }}// w lewo

        if (vector == 4) {XPosition = XPosition +0; YPosition = YPosition + 0; }//  brak ruchu

        setXPosition(XPosition);
        setYPosition(YPosition);
    }

    public void setCharactername(String charactername) {
        Charactername = charactername;
    }

    public String getCharactername() {return Charactername; }
    public int getXPosition() { return XPosition; }
    public int getYPosition() { return YPosition; }

    public void setXPosition(int XPosition) {this.XPosition = XPosition; }
    public void setYPosition(int YPosition) {this.YPosition = YPosition; }

}


