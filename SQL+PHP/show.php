<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Language" content="pl" />
  <link rel="stylesheet" href="style2.css" type="text/css" />
  <title>Zarządzanie klientem</title>
</head>
<body>
<div id="kontener_show">

<?php
include_once ('connect.php');
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany']))
    {   
        
        $c = polacz();
        $zm_log = $_SESSION['login']; 
        $id_log = $_SESSION['id']; 

        echo '<div id="linki"><a href="logout.php"> WYLOGUJ ('.$zm_log.')</a><br><br>';
        echo '<a href="index.html"> STRONA GŁÓWNA </a><br><br></div>';
        echo '<table id="table"><tr>';
        echo "<td><div id='linki'><a href=\"addf.php\"> DODAJ NOWEGO KLIENTA </a></td></div>
        </tr></table>";
        
        echo "<br><br><b>LISTA KLIENTÓW:</b> <br><br>";

    //-------------SORTOWANIE----------------------//
    $ob = (isset($_GET['ob']))? (int)$_GET['ob'] : 1;
	
	$q_ob = "";
	switch ($ob)
	{
		case 1:
		$q_ob = " ORDER BY KLIENT_ID";
		break;
		case 2:
		$q_ob = " ORDER BY IMIE";
        break;
        case 3:
		$q_ob = " ORDER BY NAZWISKO";
		break;		
		default:
		$q_ob = " ORDER BY KLIENT_ID";  
	}
	//kierunek sortowania
	if (isset($_GET['d']))
	{
		$d= (int)$_GET['d'];
	}
	else
	{
		$d=1;
	}
	
	if ($d == 1)
	{
		$q_d = " ASC";
		$nd = 0;
	}
    else
	{
		$q_d = " DESC";
		$nd = 1;
	}
//-----------------------------------------------//
       
       
        $query = "SELECT KLIENT_ID, IMIE, NAZWISKO FROM KLIENT";
        $query .= $q_ob; //
	    $query .= $q_d; //

        $stmt = oci_parse($c, $query);

//--------WYŚWIETLENIE SORTOWANIA--------------//  
?>
<html>
<div id="lista_klientow">
</html>
<?php
        oci_execute($stmt);
        echo "<table>";	
        echo "<tr>
	      <th><a href=\"show.php?ob=1&d=$nd\">ID ".(($ob==1)?(($d==1)?"UP":"DOWN"):"")."</a></th>
          <th><a href=\"show.php?ob=2&d=$nd\">IMIE ".(($ob==2)?(($d==1)?"UP":"DOWN"):"")."</a></th>
          <th><a href=\"show.php?ob=3&d=$nd\">NAZWISKO ".(($ob==3)?(($d==1)?"UP":"DOWN"):"")."</a></th>
		  </tr>";
//-----------------------------------------------//

        while ($row = oci_fetch_array($stmt))
        {
            echo "<tr>
            <td>{$row['KLIENT_ID']}</td><td>{$row['IMIE']}</td>
            <td>{$row['NAZWISKO']}</td>
            <td>
            <a href=\"delete.php?klient_id={$row['KLIENT_ID']}\"><img src=\".\img\cancel.png\" width=\"32\" title='Usuń'></a>
            <a href=\"updatef.php?klient_id={$row['KLIENT_ID']}\"><img src=\".\img\pencil.png\" width=\"32\" title='Edytuj'></a>
            </td>
            </tr>";

        }
        echo "</table>";
        
    } 
    else
    {
        header("Location: loginf.php");
    }   
    

?>
</div>
</div>
</body>
</html>