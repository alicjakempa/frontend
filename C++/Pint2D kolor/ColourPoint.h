#pragma once

#include<iostream>
#include"Point.h"

using namespace std;

class ColourPoint: public Point {
	string colour_of_point;
public:
	ColourPoint();
	ColourPoint(string);
	ColourPoint(Point, string);
	ColourPoint(float, float, string);

	string getColour();
	void setColour(string);

	void translate(float, float);
};