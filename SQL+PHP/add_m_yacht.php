<?php


include_once("connect.php");
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
	($_SESSION['zalogowany']))
	{

		$conn = polacz();

		if (isset($_POST['jacht_szczegoly_id'])) {
			$jacht_szczegoly_id = $_POST['jacht_szczegoly_id'];
		}
		if (isset($_POST['rodzaj'])) {
			$rodzaj = $_POST['rodzaj'];
		}
		if (isset($_POST['dlugosc'])) {
			$dlugosc = $_POST['dlugosc'];
        }
        if (isset($_POST['ilosc_lozek'])) {
			$ilosc_lozek = $_POST['ilosc_lozek'];
        }
        if (isset($_POST['opis'])) {
			$opis = $_POST['opis'];
        }
        
		$query = "INSERT INTO JACHT_SZCZEGOLY (JACHT_SZCZEGOLY_ID, RODZAJ, DLUGOSC, ILOSC_LOZEK, OPIS) 
        VALUES(:jacht_szczegoly_id, :rodzaj, :dlugosc, :ilosc_lozek, :opis)";
		$stm = oci_parse($conn,$query);
        
        oci_bind_by_name($stm, ':jacht_szczegoly_id', $jacht_szczegoly_id);
        oci_bind_by_name($stm, ':rodzaj', $rodzaj);
        oci_bind_by_name($stm, ':dlugosc', $dlugosc);
        oci_bind_by_name($stm, ':ilosc_lozek', $ilosc_lozek);
        oci_bind_by_name($stm, ':opis', $opis);
		
		if (oci_execute($stm))
		{		
			header("Location: lista.php");
			die();		
		}

		oci_free_statement($stm);
		oci_commit($conn);
		oci_close($conn);
	}
	else {

		header("Location: loginf.php");
	}

?>