#pragma once
#include "Praca.h"

Praca::Praca() : nazwa_zakladu("Brak danych"), zawod("Brak danych")
{
	cout << "Dziala konstruktor klasy Praca bez arg. " << endl;
}

Praca::Praca(string _nazwa_zakladu, string _zawod)
{
	cout << "Dziala konstruktor klasy Praca z arg. " << endl;
	nazwa_zakladu = _nazwa_zakladu;
	zawod = _zawod;
}

void Praca::setNazwa_zakladu(string _nazwa_zakladu) {nazwa_zakladu = _nazwa_zakladu;}

void Praca::setZawod(string _zawod) { zawod = _zawod; }

string Praca::getNazwa_zakladu() {return nazwa_zakladu;}

string Praca::getZawod() {return zawod;}
