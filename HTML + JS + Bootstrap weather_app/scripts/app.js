//DOM manipulations 

const cityForm = document.querySelector('form');
const card = document.querySelector('.card');
const details = document.querySelector('.details');
const time = document.querySelector('img.time');
const icon = document.querySelector('.icon img');
const forecast = new Forecast;

const updateUI = (data) => {
    console.log(data);
    const cityDets = data.cityDets;
    const weather = data.weather;

    //update details template
    details.innerHTML = `
    <h5 class="my-3">${cityDets.EnglishName}</h5>
    <h6 class="my-3"> ${cityDets.Country.LocalizedName}</h6>
    <div class="my-3">${weather.WeatherText}</div>
    <div class="display-4 my-4">
        <span>${weather.Temperature.Metric.Value}</span>
        <span>&deg;C</span>
    </div>`;

    //remove the d-none class if it is
    if(card.classList.contains('d-none')){
        card.classList.remove('d-none');
    };

    //update night and date image
    let timeSrc = weather.IsDayTime ? 'img/day.svg' : 'img/night.svg';
    time.setAttribute('src', timeSrc);

    //update weather icon
    let number = weather.WeatherIcon;
    icon.setAttribute('src', `img/icons/${number}.svg`);
}

cityForm.addEventListener('submit', e => {
    //prevent defoult actions
    e.preventDefault();

    //get city value without extra spaces - trim()
    const city = cityForm.city.value.trim();
    cityForm.reset();

    //update the ui with new city
    forecast.updateCity(city)
    .then(data => updateUI(data))
    .catch(err => console.log(err));

    //set localStorage
    localStorage.setItem('city', city);
});

if(localStorage.getItem('city')){
    forecast.updateCity(localStorage.getItem('city'))
    .then(data => updateUI(data))
    .catch(err => console.log(err));
}



