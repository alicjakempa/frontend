#include "Zwierze.h"
#include "Rasa.h"

Zwierze::Zwierze() : imie ("Brak danych"), nr_chip(0)
{
	cout << "Dziala konstruktor klasy Zwierze bez arg. " << endl;
}

Zwierze::Zwierze(string _imie, int _nr_chip)
{ 
	imie = _imie;
	nr_chip = _nr_chip;
	cout << "Dziala konstruktor klasy Zwierze z 2 arg. " << endl;
}


void Zwierze::setImie(string _imie) {imie = _imie;}

void Zwierze::setNr_chip(int _nr_chip) {nr_chip = _nr_chip;}

string Zwierze::getImie() {return imie;}

int Zwierze::getNr_chip() {return nr_chip;}
