<?php
include_once('connect.php');
session_start();
if (
(isset($_SESSION['zalogowany']))
	&&
($_SESSION['zalogowany'])
)
{
    $conn = polacz();
    $jacht_szczegoly_id = $_GET['jacht_szczegoly_id'];
	
	$query = "SELECT * FROM JACHT_SZCZEGOLY WHERE JACHT_SZCZEGOLY_ID=:jacht_szczegoly_id";
	
	$stmt = oci_parse($conn,$query);
	
   	oci_bind_by_name($stmt,':jacht_szczegoly_id',$jacht_szczegoly_id);
		
	oci_execute($stmt);
				
	$row = oci_fetch_array($stmt);
	
	if (!$row)
	{
		echo "Błąd, nie działa!";
	}
	
    include_once("update_m_yacht_html.php");
}	
?>