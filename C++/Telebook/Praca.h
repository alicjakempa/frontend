#pragma once
#include<iostream>

using namespace std;

class Praca {
	string nazwa_zakladu;
	string zawod;

public:
	Praca();
	Praca(string _nazwa_zakladu, string _zawod);

	void setNazwa_zakladu(string);
	void setZawod(string);
	
	string getNazwa_zakladu();
	string getZawod();

};