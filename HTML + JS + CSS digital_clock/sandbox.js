const clock = document.querySelector('.clock');
const japanClock = document.querySelector('.japan_clock');
const nyClock = document.querySelector('.ny_clock');

const tick = () => {

    let now = new Date().toLocaleString("en-US", {timeZone: 'Europe/Warsaw' });
    now = new Date(now);

    const now_date = now.toLocaleDateString();
    const h = now.getHours();
    const m = now.getMinutes();
    const s = now.getSeconds();

    const html = `
    <span class="nobg">${now_date}</span><br><br>
    <span>${h}</span> :
    <span>${m}</span> :
    <span>${s}</span>
    `;

    clock.innerHTML = html;
};

const tick_japan = () => {

    let now_japan = new Date().toLocaleString("en-US", {timeZone: 'Asia/Tokyo' });
    now_japan = new Date(now_japan);
   
    const now_date = now_japan.toLocaleDateString();
    const h = now_japan.getHours();
    const m = now_japan.getMinutes();
    const s = now_japan.getSeconds();

    const html = `
    <span class="nobg">${now_date}</span><br><br>
    <span>${h}</span> :
    <span>${m}</span> : 
    <span>${s}</span>
    `;

    japanClock.innerHTML = html;
};

const tick_ny = () => {

    let now_ny = new Date().toLocaleString("en-US", {timeZone: 'America/New_York' });
    now_ny = new Date(now_ny);
   
    const now_date = now_ny.toLocaleDateString();
    const h = now_ny.getHours();
    const m = now_ny.getMinutes();
    const s = now_ny.getSeconds();

    const html = `
    <span class="nobg">${now_date}</span><br><br>
    <span>${h}</span> :
    <span>${m}</span> : 
    <span>${s}</span>
    `;

    nyClock.innerHTML = html;
};

setInterval(tick, 1000);
setInterval(tick_japan, 1000);
setInterval(tick_ny, 1000);


