<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Language" content="pl" />
  <link rel="stylesheet" href="style2.css" type="text/css" />
  <title>Lista jachtów</title>
</head>
<body>
<div id="kontener_lista">

<?php
include_once ('connect.php');
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany']))
    {   
        
        $c = polacz();
        $zm_log = $_SESSION['login']; 
        $id_log = $_SESSION['id']; 

        echo '<div id="linki"><a href="logout.php"> WYLOGUJ ('.$zm_log.')</a><br>';
        echo '<a href="index.html"> STRONA GŁÓWNA </a><br></div>';
        echo '<table id="table"><tr>';
        echo "<tr><td><div id='linki'><a href=\"addf_yacht.php\">DODAJ NOWY JACHT DO WYPOŻYCZEŃ: </a></td></div><tr>";
        echo "<tr><td><div id='linki'><a href=\"addf_m_yacht.php\">DODAJ NOWY MODEL/ RODZAJ JACHTU DO KATALOGU: </a></td></div></tr></table>";
        echo "<br><br><b>LISTA JACHTÓW DO WYPOŻYCZEŃ:</b> <br><br>";

    //-------------SORTOWANIE LISTY JACHTÓW----------------------//
    $ob = (isset($_GET['ob']))? (int)$_GET['ob'] : 1;
	
	$q_ob = "";
	switch ($ob)
	{
		case 1:
		$q_ob = " ORDER BY JACHT_ID";
		break;
		case 2:
		$q_ob = " ORDER BY NAZWA";
        break;
        case 3:
		$q_ob = " ORDER BY JACHT_SZCZEGOLY_ID";
        break;
        
		default:
		$q_ob = " ORDER BY JACHT_ID";  
	}
	//kierunek sortowania
	if (isset($_GET['d']))
	{
		$d= (int)$_GET['d'];
	}
	else
	{
		$d=1;
	}
	if ($d == 1)
	{
		$q_d = " ASC";
		$nd = 0;
	}
    else
	{
		$q_d = " DESC";
		$nd = 1;
	}
//-----------------------------------------------//
       
        $query = "SELECT JACHT_ID, NAZWA, JACHT_SZCZEGOLY_ID FROM JACHT";
        $query .= $q_ob; 
        $query .= $q_d; 
        $stmt = oci_parse($c, $query);

//--------WYŚWIETLENIE SORTOWANIA JACHTÓW--------------//  
?>
<html>
<div id="lista_jachtow">
</html>
<?php
        oci_execute($stmt);
        echo "<table>";	
        echo "<tr>
	      <th><a href=\"lista.php?ob=1&d=$nd\">ID JACHTU ".(($ob==1)?(($d==1)?"UP":"DOWN"):"")."</a></th>
          <th><a href=\"lista.php?ob=2&d=$nd\">NAZWA ".(($ob==2)?(($d==1)?"UP":"DOWN"):"")."</a></th> 
          <th><a href=\"lista.php?ob=3&d=$nd\">MODEL Z KATALOGU ".(($ob==3)?(($d==1)?"UP":"DOWN"):"")."</a></th> 
		  </tr>";
//-----------------------------------------------//
        while ($row = oci_fetch_array($stmt))
        {
            echo "<tr>
            <td>{$row['JACHT_ID']}</td>
            <td>{$row['NAZWA']}</td>
            <td>{$row['JACHT_SZCZEGOLY_ID']}</td>
            <td><a href=\"delete_yacht.php?jacht_id={$row['JACHT_ID']}\"><img src=\".\img\cancel.png\" width=\"32\"></a>
            <a href=\"updatef_yacht.php?jacht_id={$row['JACHT_ID']}\"><img src=\".\img\pencil.png\" width=\"32\"></a> </td>
            </tr>";

        }
        echo "</table>";

        echo "<br><br><b>LISTA MODELI JACHTÓW W KATALOGU: </b><br><br>";

        //-------------SORTOWANIE LISTY MODELI----------------------//
    $ob2 = (isset($_GET['ob2']))? (int)$_GET['ob2'] : 1;
	
	$q_ob2 = "";
	switch ($ob2)
	{
		case 1:
		$q_ob2 = " ORDER BY JACHT_SZCZEGOLY_ID";
		break;
		case 2:
		$q_ob2 = " ORDER BY RODZAJ";
        break;
        case 3:
        $q_ob2 = " ORDER BY DLUGOSC";
        break;
        case 4:
        $q_ob2 = " ORDER BY ILOSC_LOZEK";
        break;
        
		default:
		$q_ob2 = " ORDER BY JACHT_SZCZEGOLY_ID";  
	}
	//kierunek sortowania
	if (isset($_GET['d2']))
	{
		$d2= (int)$_GET['d2'];
	}
	else
	{
		$d2=1;
	}
	if ($d2 == 1)
	{
		$q_d2 = " ASC";
		$nd2 = 0;
	}
    else
	{
		$q_d2 = " DESC";
		$nd2 = 1;
	}
//-----------------------------------------------//
       
        $query2 = "SELECT * FROM JACHT_SZCZEGOLY";
        $query2 .= $q_ob2; 
        $query2 .= $q_d2; 
        $stmt2 = oci_parse($c, $query2);

//--------WYŚWIETLENIE SORTOWANIA JACHTÓW--------------//  
?>
<html>
<div id="lista_klientow">
</html>
<?php
        oci_execute($stmt2);
        echo "<table>";	
        echo "<tr>
	      <th><a href=\"lista.php?ob2=1&d2=$nd2\">ID JACHTU ".(($ob2==1)?(($d2==1)?"UP":"DOWN"):"")."</a></th>
          <th><a href=\"lista.php?ob2=2&d2=$nd2\">RODZAJ/MODEL ".(($ob2==2)?(($d2==1)?"UP":"DOWN"):"")."</a></th> 
          <th><a href=\"lista.php?ob2=3&d2=$nd2\">DLUGOŚĆ [M] ".(($ob2==3)?(($d2==1)?"UP":"DOWN"):"")."</a></th> 
          <th><a href=\"lista.php?ob2=4&d2=$nd2\">ILOŚĆ MIEJSC DO SPANIA ".(($ob2==4)?(($d2==1)?"UP":"DOWN"):"")."</a></th> 
          <th> OPIS </th> 
		  </tr>";
//-----------------------------------------------//
        while ($row2 = oci_fetch_array($stmt2))
        {
            echo "<tr>
            <td>{$row2['JACHT_SZCZEGOLY_ID']}</td>
            <td>{$row2['RODZAJ']}</td>
            <td>{$row2['DLUGOSC']}</td>
            <td>{$row2['ILOSC_LOZEK']}</td>
            <td id='krotki'>{$row2['OPIS']}</td>
            <td><a href=\"delete_m_yacht.php?jacht_szczegoly_id={$row2['JACHT_SZCZEGOLY_ID']}\"><img src=\".\img\cancel.png\" width=\"32\"></a>
            <a href=\"updatef_m_yacht.php?jacht_szczegoly_id={$row2['JACHT_SZCZEGOLY_ID']}\"><img src=\".\img\pencil.png\" width=\"32\"></a> </td>
            </tr>";

        }
        echo "</table>";
        
    }   
    else
    {
        header("Location: loginf.php");
    }  

?>
</div>
</div>
</body>
</html>