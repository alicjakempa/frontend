#pragma once

#include<iostream>
#include"Point.h"

using namespace std;

Point::Point() {
	cout << "Dziala konstruktor klasy Point bez arg.\n";
	x = 0.0;
	y = 0.0;
}

Point::Point(float wspX, float wspY) : x(wspX), y(wspY) {
	cout << "Dziala konstruktor klasy Point z arg.\n";
}

Point::Point(float aaa) : x(aaa), y(aaa) {
	cout << "Dziala konstruktor klasy Point z jednym arg.\n";
}

Point::Point(float wspX, float wspY, int cw){
	cout << "Dziala konstruktor klasy Point z trzema arg.\n";

	if (cw < 1 || cw > 4) {
		cout << "Podano zle dane\n";
		cout << "Obiekt zostanie zainicjalizowany domyslnymi danymi\n";
		x = 0.0;
		y = 0.0;
	}
	else {
		if (wspX >= 0.0 && wspY >= 0.0 && cw == 1) {
			x = wspX;
			y = wspY;
		}
		else if (wspX < 0.0 && wspY > 0.0 && cw == 2) {
			x = wspX;
			y = wspY;
		}
		else if (wspX < 0.0 && wspY < 0.0 && cw == 3) {
			x = wspX;
			y = wspY;
		}
		else if (wspX > 0.0 && wspY < 0.0 && cw == 4) {
			x = wspX;
			y = wspY;
		}
		else {
			cout << "Podano zle dane\n";
			cout << "Obiekt zostanie zainicjalizowany domyslnymi danymi\n";
			x = 0.0;
			y = 0.0;
		}
	}
}

void Point::setX(float a) { x = a; }
void Point::setY(float b) { y = b; }

float Point::getX() { return x; }
float Point::getY() { return y; }

void Point::nr_cw(){
	if (x >= 0.0 && y >= 0.0) {
		cout << "Punkt lezy w 1 cwiartce\n";
	}
	else if (x < 0.0 && y > 0.0) {
		cout << "Punkt lezy w 2 cwiartce\n";
	}
	else if (x < 0.0 && y < 0.0) {
		cout << "Punkt lezy w 3 cwiartce\n";
	}
	else if (x > 0.0 && y < 0.0) {
		cout << "Punkt lezy w 4 cwiartce\n";
	}
}

float Point::distance(Point p1)
{
	return sqrtf( (x - p1.x)*(x - p1.x) \
		+ powf((y - p1.y), 2) );
}

void Point::translate(float a, float b)
{
	x += a; // x = x+a
	y += b; // y = y+b
}
