#pragma once
#include<iostream>

using namespace std;

class Adres {
	string miasto;
	string ulica;
	int nr_budynku;
	int nr_mieszkania;

public:
	Adres();
	Adres(string _miasto, string _ulica,int _nr_budynku,int _nr_mieszkania);

	void setMiasto(string);
	void setUlica(string);
	void setNr_budynku(int);
	void setNr_mieszkania(int);

	string getMiasto();
	string getUlica();
	int getNr_budynku();
	int getNr_mieszkania();


};