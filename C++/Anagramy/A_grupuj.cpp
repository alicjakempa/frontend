#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#define WORDMAX 100

using namespace std;


int main(void) {
	char word[WORDMAX];
	char sig[WORDMAX];
	char oldsig[WORDMAX];
	int linenum = 0;

	strcpy(oldsig, "");

	while (scanf("%s %s", sig, word) != EOF) {
		if (strcmp(oldsig, sig) != 0 && linenum > 0) {
			printf("\n");
			strcpy(oldsig, sig);
			linenum++;
			printf("\n", word);
		}
		printf("\n");
		return 0;
	}
}