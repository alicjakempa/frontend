<?php
include_once("connect.php");

session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany'])
    ){
        echo "<center>Jesteś już zalogowany!</center>";
    }
    else {
        echo "<center><br>Nie jesteś zalogowany. Zaloguj się</center>";
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Formularz logowania</title>
        <link rel="stylesheet" href="style2.css" type="text/css">
        
    </head>
    <body>
        <div id="kontener_log">
        <form action="login.php" method="post">
                
            <div id="logowanie">
                Login:<br><input type="text" name="login"><br>
                Hasło:<br><input type="password" name="pass"><br><br>
            </div>  
            <div id="zaloguj">  
                <input type="submit" value="Zaloguj">
            </div>
        </form>
        </div>
    </body>

</html>