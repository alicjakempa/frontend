<?php
include_once ('connect.php');
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany']))
    {   
   
	$conn = polacz();
	
	$id = $_GET['jacht_szczegoly_id'];
	
	$query  = "SELECT * FROM JACHT WHERE JACHT_SZCZEGOLY_ID=:id";
	$stm = oci_parse($conn, $query);
	oci_bind_by_name($stm, ':id', $id);
	
	oci_execute($stm);
        
		if($row = oci_fetch_array($stm)){
		echo "Nie można usunąć jachtu. Dany model przypisany jest do jachtu/ów będącym/ych w flocie. <br>Zmodyfikuj jachty przed usunięciem modelu.
        <br><br>Lista jachtów przypisująca usuwany model:<br><br>";
		echo "ID JACHTU: <b>".$row[0]."</b>    NAZWA: ".$row[1]."<br><br>";
		echo "<a href='lista.php'>Zarządzanie jachtami</a>";
		}
		else{

			$query2 = "DELETE JACHT_SZCZEGOLY WHERE JACHT_SZCZEGOLY_ID=:id";
			
			$stm2 = oci_parse($conn,$query2);
			
			oci_bind_by_name($stm2, ':id', $id);
		
		
			if (oci_execute($stm2))
			{	
				header("Location: lista.php");
				die();			
			}
		}
}

else{
	echo "Błąd krytyczny";
}



?>