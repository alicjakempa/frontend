<?php

include_once("connect.php");
session_start();
if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany'])){

	$conn = polacz();
	
	if (isset($_POST['rezerwacje_id'])) {
		$rezerwacje_id = $_POST['rezerwacje_id'];}
	if (isset($_POST['klient_id'])) {
		$klient_id = $_POST['klient_id'];}
	if (isset($_POST['jacht_id'])) {
		$jacht_id = $_POST['jacht_id'];}
	if (isset($_POST['start_rezerwacji'])) {
		$start_rezerwacji = $_POST['start_rezerwacji'];}
	if (isset($_POST['koniec_rezerwacji'])) {
		$koniec_rezerwacji = $_POST['koniec_rezerwacji'];}

	$query = "UPDATE REZERWACJE SET KLIENT_ID=:klient_id, JACHT_ID=:jacht_id, 
    START_REZERWACJI= TO_DATE('$start_rezerwacji','YYYY-MM-DD'), KONIEC_REZERWACJI=TO_DATE('$koniec_rezerwacji','YYYY-MM-DD') 
	WHERE REZERWACJE_ID=:rezerwacje_id";

	$stm = oci_parse($conn, $query);
	
	oci_bind_by_name($stm, ':rezerwacje_id', $rezerwacje_id);
    oci_bind_by_name($stm, ':klient_id', $klient_id);
    oci_bind_by_name($stm, ':jacht_id', $jacht_id);
    oci_bind_by_name($stm, ':start_rezerwacji', $start_rezerwacji);
    oci_bind_by_name($stm, ':koniec_rezerwacji', $koniec_rezerwacji);

	
		if (oci_execute($stm,OCI_NO_AUTO_COMMIT))
		{	
			header("Location: rezerwacje.php");
							
		}
		else {
		echo "błąd krytyczny";
		}
	oci_free_statement($stm);
	oci_commit($conn);
	oci_close($conn);	
}
?>