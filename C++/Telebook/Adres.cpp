#include "Adres.h"

Adres::Adres()
{
	cout << "Dzia�a kanstruktor bez argumentow" << endl;
	miasto = "Brak danych";
	ulica = "Brak danych";
}

Adres::Adres(string _miasto, string _ulica, int _nr_budynku, int _nr_mieszkania) {
	 miasto = _miasto;
	 ulica = _ulica;
	 nr_budynku = _nr_budynku;
	 nr_mieszkania = _nr_mieszkania;
}

void Adres::setMiasto(string m) {  miasto = m; }

void Adres::setUlica(string u) { ulica = u; }

void Adres::setNr_budynku(int nr_b) { nr_budynku = nr_b; }

void Adres::setNr_mieszkania(int nr_m) { nr_mieszkania = nr_m; }

string Adres::getMiasto() {return miasto;}

string Adres::getUlica() {return ulica;}

int Adres::getNr_budynku() {return nr_budynku;}

int Adres::getNr_mieszkania() {return nr_mieszkania;}
