#pragma once
#include "ColourPoint.h"

ColourPoint::ColourPoint()
{
	cout << "Dziala konstruktor klasy ColourPoint bez arg.\n";
	colour_of_point = "red";
}

ColourPoint::ColourPoint(string col):colour_of_point(col)
{
	cout << "Dziala konstruktor klasy ColourPoint z 1 arg.\n";
}

ColourPoint::ColourPoint(Point p, string col) : Point(p), colour_of_point(col)
{
	//x = p.getX();
	//y = p.getY();
	cout << "Dziala konstruktor klasy ColourPoint z 2 arg.\n";
}

ColourPoint::ColourPoint(float wspx, float wspy, string col):Point(wspx,wspy)
{
	cout << "Dziala konstruktor klasy ColourPoint z 3 arg.\n";
	//x = wspx;
	//y = wspy;
	colour_of_point = col;
}

string ColourPoint::getColour() { return colour_of_point; }

void ColourPoint::setColour(string col) { colour_of_point = col; }

void ColourPoint::translate(float a, float b)
{
	x += 2*a; // x = x+a
	y += 2*b; // y = y+b
}
