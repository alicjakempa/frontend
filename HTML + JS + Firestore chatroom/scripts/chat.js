// adding new chat documents
//setting up a real-time listener to get chats

class Chatroom {
    constructor(room, username){
        this.room = room;
        this.username = username;
        this.chats = db.collection('chats');
        this.unsub;
    }
    async addChat(message){
        //format a chat object
        const now = new Date();
        const chat = {
            message: message,
            username: this.username,
            room: this.room,
            created_at: firebase.firestore.Timestamp.fromDate(now)
        };
        // save a chat document to db
        const response = await this.chats.add(chat);
        return response;
    }
    //query selector - where function. Takes 3 parameters, for us is where room is equal to this.room
    //realtime listener - onSnapshot function
    getChats(callback){
       this.unsub = this.chats
        .where('room', '==', this.room)
        .orderBy('created_at')
        .onSnapshot( snapshot => {
            snapshot.docChanges().forEach(change => {
                if(change.type === 'added'){
                    callback(change.doc.data())
                };
            });
        });
    };
    //update username
    updateName(username){
        this.username = username;
        localStorage.setItem('username', username);
    };
    // update room name
    updateRoom(room){
        this.room = room;
        console.log('room updated');
        if(this.unsub){
        this.unsub()
        };
    }
}
