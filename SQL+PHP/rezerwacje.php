<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Language" content="pl" />
  <link rel="stylesheet" href="style2.css" type="text/css" />
  <title>Zarządzanie rezerwacją</title>
</head>
<body>
<div id="kontener_show">


<?php
include_once ('connect.php');
session_start();

if (
    (isset($_SESSION['zalogowany']))
        &&
    ($_SESSION['zalogowany']))
    {   
        
        $c = polacz();
        $zm_log = $_SESSION['login']; 
        $id_log = $_SESSION['id']; 

        echo '<div id="linki"><a href="logout.php"> WYLOGUJ ('.$zm_log.')</a><br>';
        echo '<a href="index.html"> STRONA GŁÓWNA </a></div><br>';
        echo '<table><tr>';
        echo "<td><div id='linki'><a href=\"addf_reservation.php\"> DODAJ NOWĄ REZERWACJĘ: </a></td></div></tr></table>";
        
        echo "<br><br><b> LISTA REZERWACJI: </b> <br><br>";

    //-------------SORTOWANIE----------------------//
    $ob = (isset($_GET['ob']))? (int)$_GET['ob'] : 1;
	
	$q_ob = "";
	switch ($ob)
	{
		case 1:
		$q_ob = " ORDER BY REZERWACJE_ID";
		break;
		case 2:
		$q_ob = " ORDER BY KLIENT_ID";
        break;
        case 3:
		$q_ob = " ORDER BY JACHT_ID";
        break;	
        case 4:
		$q_ob = " ORDER BY START_REZERWACJI";
        break;	
        case 5:
		$q_ob = " ORDER BY KONIEC_REZERWACJI";
        break;	
        	
		default:
		$q_ob = " ORDER BY REZERWACJE_ID";  
	}
	//kierunek sortowania
	if (isset($_GET['d']))
	{
		$d= (int)$_GET['d'];
	}
	else
	{
		$d=1;
	}
	
	if ($d == 1)
	{
		$q_d = " ASC";
		$nd = 0;
	}
    else
	{
		$q_d = " DESC";
		$nd = 1;
	}
//-----------------------------------------------//
       
       
        $query = "SELECT REZERWACJE_ID, KLIENT_ID, JACHT_ID, START_REZERWACJI, KONIEC_REZERWACJI FROM REZERWACJE";
       
        $query .= $q_ob; //
	    $query .= $q_d; //

        $stmt = oci_parse($c, $query);

//--------WYŚWIETLENIE SORTOWANIA--------------//  
?>
<html>
<div id="lista_klientow">
</html>
<?php
        oci_execute($stmt);
        echo "<table>";	
        echo "<tr>
	      <th><a href=\"rezerwacje.php?ob=1&d=$nd\">ID REZERWACJI ".(($ob==1)?(($d==1)?"UP":"DOWN"):"")."</a></th>
          <th><a href=\"rezerwacje.php?ob=2&d=$nd\">ID KLIENTA ".(($ob==2)?(($d==1)?"UP":"DOWN"):"")."</a></th>
          <th><a href=\"rezerwacje.php?ob=3&d=$nd\">ID JACHTU ".(($ob==3)?(($d==1)?"UP":"DOWN"):"")."</a></th>
          <th><a href=\"rezerwacje.php?ob=4&d=$nd\">START REZERWACJI ".(($ob==4)?(($d==1)?"UP":"DOWN"):"")."</a></th>
          <th><a href=\"rezerwacje.php?ob=5&d=$nd\">KONIEC REZERWACJI ".(($ob==5)?(($d==1)?"UP":"DOWN"):"")."</a></th>
		  </tr>";
//-----------------------------------------------//

       

        while ($row = oci_fetch_array($stmt))
        {
            echo "<tr>
            <td>{$row['REZERWACJE_ID']}</td><td>{$row['KLIENT_ID']}</td>
            <td>{$row['JACHT_ID']}</td><td>{$row['START_REZERWACJI']}</td><td>{$row['KONIEC_REZERWACJI']}</td>
            <td>
            <a href=\"delete_reservation.php?rezerwacje_id={$row['REZERWACJE_ID']}\"><img src=\".\img\cancel.png\" width=\"32\" title='Usuń'></a>
            <a href=\"updatef_reservation.php?rezerwacje_id={$row['REZERWACJE_ID']}\"><img src=\".\img\pencil.png\" width=\"32\" title='Zmień'></a>
            </td>
            </tr>";

        }
        echo "</table>";
        
    } 
    else
    {
        header("Location: loginf.php");
    }   


?>
</div>
</div>
</body>
</html>