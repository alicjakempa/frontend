package kota.nie.ma.ala;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class Game {
    //wywolanie mapy
    public void start() {
        Map mapStart = new Map();
        mapStart.default_map();
        mapStart.getMap();

        int cats_number = 4;
        int mouse_number = 4;
        int roundNumber = 10000000;
        int DrawBoardPiriot = 10;

        List<Cat> cats = new ArrayList<>();
        List<Mouse> mouses = new ArrayList<>();
        List<Cheese> cheeses = new ArrayList<>();

        ////////// wywolanie na mape punktu kolizyjnego ////////
        Colision colision_point = new Colision(5,8,"X");
        List<Colision> ColisionList = new ArrayList<>();
        ColisionList.add(colision_point);
        mapStart.map[colision_point.getXPosition()][colision_point.getYPosition()] = "X";

        for (int k = 1; k < 2; k++) {
            Cheese cheese = new Cheese();
            cheeses.add(cheese);
            mapStart.map[cheese.getXPosition()][cheese.getYPosition()] = cheese.getCharactername();
        }

        for (int i = 1; i < cats_number + 1; i++) {
            Cat cat = new Cat();
            cat.setCharactername(i + cat.getCharactername());
            cats.add(cat);
            mapStart.map[cat.getXPosition()][cat.getYPosition()] = cat.getCharactername();
        }
        for (int j = 1; j < mouse_number + 1; j++) {
            Mouse mouse = new Mouse();
            mouse.setCharacterName(j + mouse.getCharacterName());
            mouses.add(mouse);
            mapStart.map[mouse.getXPosition()][mouse.getYPosition()] = mouse.getCharacterName();
        }
        mapStart.print_map();
        System.out.println("_________________________________");


        //tura 1



        for (int r = 1; r < roundNumber; r++) {
            System.out.println("\nRound : " + r);

            mapStart.default_map();

            for (Cheese cheese : cheeses) {
                mapStart.map[cheese.getXPosition()][cheese.getYPosition()] = cheese.getCharactername();
            }

            for (Mouse mouse : mouses) {
                int tmpX = mouse.getXPosition();
                int tmpY = mouse.getYPosition();

                System.out.print("Mouse: " + mouse.getCharacterName() + " X,Y " + mouse.getXPosition() + "," + mouse.getYPosition() + " --> ");
                mouse.move();
                if((mouse.getXPosition() == colision_point.getXPosition()) & (mouse.getYPosition() == colision_point.getYPosition()) ){
                    mouse.setXPosition(tmpX);
                    mouse.setYPosition(tmpY);
                }
                System.out.println(mouse.getXPosition() + "," + mouse.getYPosition());
                mapStart.map[mouse.getXPosition()][mouse.getYPosition()] = mouse.getCharacterName();
            }

            for (Cat cat : cats) {
                int tmpX = cat.getXPosition();
                int tmpY = cat.getYPosition();
                System.out.print("Cat: " + cat.getCharactername() + " X,Y " + cat.getXPosition() + "," + cat.getYPosition() + " --> ");
                cat.move();
                if((cat.getXPosition() == colision_point.getXPosition()) & (cat.getYPosition() == colision_point.getYPosition()) ){
                    cat.setXPosition(tmpX);
                    cat.setYPosition(tmpY);
                }

                    System.out.println(cat.getXPosition() + "," + cat.getYPosition());
                    mapStart.map[cat.getXPosition()][cat.getYPosition()] = cat.getCharactername();

            }

            mapStart.map[colision_point.getXPosition()][colision_point.getYPosition()] = "X";


////////////////////RUCH - MYSZ ZDOBYWA GOUDE lub remis///////
            for (Mouse mouse : mouses) {
                for (Iterator<Cheese> it = cheeses.iterator(); ((Iterator) it).hasNext(); ) {
                    Cheese cheese = it.next();
                    if (mouse.getXPosition() == cheese.getXPosition() & mouse.getYPosition() == cheese.getYPosition()) {
                        it.remove();
                        for (Iterator<Cat> that = cats.iterator(); that.hasNext(); ) {
                            Cat cat = that.next();
                            if (mouse.getXPosition() == cat.getXPosition() & mouse.getYPosition() == cat.getYPosition()) {
                                that.remove();
                                System.out.println("REMIS");
                            }
                        }
                        System.out.println("Mouse " + mouse.getCharacterName() + " got Gouda. Mouses win! ");
                        System.exit(0);
                    }
                }
            }

            //////MYSZY ZJADAJA KOTA ///////


            String[][] checkMap = new String[mapStart.getMap_size()][mapStart.getMap_size()];
            int i = 0, j = 0;
            for (i = 0; i < checkMap.length; i++) {


                for (j = 0; j < checkMap.length; j++) {
                    int X;
                    int Y;
                    X=i;
                    Y=j;

                    List<Mouse> mouseTable = new ArrayList<>();
                    List<Cat> catTable = new ArrayList<>();

                    for (Mouse mouse : mouses) {

                        if (mouse.getXPosition() == X & mouse.getYPosition() == Y) {
                            mouseTable.add(mouse);
                        }
                    }
                    for (Cat cat : cats) {

                        if (cat.getXPosition() == X & cat.getYPosition() == Y) {
                            catTable.add(cat);
                        }
                    }

                    int MouseNumber = mouseTable.size();
                    int CatNumber = catTable.size();

                    if ((MouseNumber >= (2*CatNumber)) & (CatNumber != 0)) {
                        int CatRandom = ((int) ((Math.random()*1000)) % mouseTable.size()-1);
                        String CatToRemove = catTable.get(CatRandom).getCharactername();
                        for (Cat cat : cats) {
                            if (CatToRemove == cat.getCharactername()){

                                System.out.println("Cat " + cat.getCharactername() + " was eaten by mouses !!!!!!!!!!!!!!!!!!!!! " );
                                cats.remove(cat);
                            }
                        }
                    }
                }
            }

/////////////////////// RUCH - KOT ZJADA MYSZ ////
            for (Cat cat : cats) {
                for (Iterator<Mouse> it = mouses.iterator(); ((Iterator) it).hasNext(); ) {
                    Mouse mouse = it.next();
                    if (mouse.getXPosition() == cat.getXPosition() & mouse.getYPosition() == cat.getYPosition()) {
                        it.remove();
                        System.out.println("Cat " + cat.getCharactername() + " ate mouse " + mouse.getCharacterName());
                    }
                }
            }



            //////////////KOTY WYGRALY /////
            if (mouses.isEmpty()) {
                System.out.println(" Cats win ! ");
                System.exit(0);
            }
            //// CZESTOSLIWOSC WYSWIELTANIA POLA GRY ////

            if (r % DrawBoardPiriot == 0){
            mapStart.print_map();}
        }

    }
}

